# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete(("a".."z").to_a.join)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle_char = str.length/2
  if str.length.even?
    str[middle_char-1]+str[middle_char]
  else
    str[middle_char]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_count = 0
  str.each_char do |x|
    vowel_count += 1 if VOWELS.include?(x)
  end
  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  arr.each_with_index do |x, index|
    index == arr.length - 1 ? string << x : string << (x + separator)
  end
  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weirdcase_word = ""
  str.downcase.chars.each_with_index do |letter, index|
    index.even? ? weirdcase_word << letter : weirdcase_word << letter.upcase
  end
  weirdcase_word
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  array = []
  str.split.each do |word|
    word.length > 4 ? array << word.reverse : array << word
  end
  array.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = []
  (1..n).to_a.each do |n|
    if n % 3 == 0 && n % 5 == 0
      array << "fizzbuzz"
    elsif n % 3 == 0
      array << "fizz"
    elsif n % 5 == 0
      array << "buzz"
    else
      array << n
    end
  end
  array
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  factors(num).length == 2
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).to_a.each do |int|
    factors << int if num%int == 0
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []
  factors(num).each {|int| prime_factors << int if prime?(int)}
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_even = 0
  arr.each {|int| int.odd? ? odd_even -= 1 : odd_even += 1}
  arr.each do |int|
    if odd_even > 0
      return int if int.odd?
    else
      return int if int.even?
    end
  end
end
